.. Filename:          SpecClient.SpecChannel.txt
.. Description:       
.. Author:            Clement Buton <clement.buton@esrf.fr>
.. Author:            $Author: cbuton $
.. Created on:        $Date: 2013/07/31 13:53:15 $ 
.. Modified on:       2013/07/31 14:04:20
.. Copyright:         2013, Clement BUTON 
.. $Id: SpecClient.SpecChannel.txt, 2013/07/31 13:53:15 cbuton Exp $

.. automodule:: ToolBox.SpecClient.SpecChannel
   :members:
   :undoc-members:
