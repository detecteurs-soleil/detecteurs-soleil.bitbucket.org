.. Filename:          libCalibration.txt
.. Description:       Sphinx autodoc
.. Author:            Clement Buton <clement.buton@esrf.fr>
.. Author:            $Author: cbuton $
.. Created on:        $Date: 2013/02/13 16:52:17 $ 
.. Modified on:       2013/03/13 10:28:31
.. Copyright:         2013, Clement BUTON 
.. $Id: libCalibration.txt, 2013/02/13 16:52:17 cbuton Exp $

.. automodule:: Calibration.libCalibration
   :members:
   :undoc-members:
