.. Filename:          optimizeCalibration.txt
.. Description:       Sphinx autodoc
.. Author:            Clement Buton <clement.buton@synchrotron-soleil.fr>
.. Author:            $Author: cbuton $
.. Created on:        $Date: 2013/07/23 09:56:01 $
.. Modified on:       2014/06/24 15:04:17
.. Copyright:         2014, Clement BUTON
.. $Id: optimizeCalibration.txt, 2013/07/23 09:56:01 cbuton Exp $

.. automodule:: optimizeCalibration
   :members:
   :undoc-members:
