#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
## Filename:          cppm_analysis.py
## Description:       script description
## Author:            Clement Buton <clement.buton@synchrotron-soleil.fr>
## Author:            $Author: cbuton $
## Created on:        $Date: 2013/09/03 14:40:05 $
## Modified on:       2014/07/08 13:23:43
## Copyright:         2013, Clément Buton (CeCILL-C)
## $Id: cppm_analysis.py, 2013/09/03 14:40:05 cbuton Exp $
###############################################################################

"""
.. _cppm_analysis.py:

cppm_analysis.py
================

**Analysis of the data taken at Cppm on 08/2013.**

.. todo::

"""

from __future__ import print_function  # be prepared for python 3.x

__author__ = 'Clement Buton <clement.buton@synchrotron-soleil.fr>'
__date__ = '2013/09/03 14:40:05'
__adv__ = 'cppm_analysis.py'

import os
import datetime
import numpy as N
import ToolBox.ColoredText as TC

# Non-default colors
blue = '#0066CC'
red = '#CC0033'
green = '#009966'
orange = '#FF9900'
purple = '#FF00FF'

# MAIN ########################################################################

if __name__ == "__main__":

    start_time = datetime.datetime.now()  # start time

    import optparse

    # Options =================================================================

    usage = " usage: [%prog] [options] \n" \
            " [-g graph -p plot -V verbose -D debug --dpi] "

    description = TC.strc("""
Analysis of the energy range of a DAC from the data taken at Cppm on
16/08/2013.""", 'black', 'bold')

    parser = optparse.OptionParser(usage, description=description)

    # Inputs/Outputs ------------------------------

    io = optparse.OptionGroup(parser, TC.strc("Inputs/Outputs",
                                              'blue', 'bold'))

    io.add_option('-d', '--dataPath', type='string',
                  help="Data path. ['%default']",
                  default='/Users/cbuton/Projects/Xpad/Data/CPPM/CPPM_23_08_13')

    parser.add_option_group(io)

    # Detector ------------------------------

    det = optparse.OptionGroup(parser, TC.strc("Detector caracteristics",
                                               'blue', 'bold'))

    det.add_option('--chipShape', type='int', nargs=2,
                   help="Chip shape (rows, columns) in pixels. [%default]",
                   default=(120, 80))
    det.add_option('-n', '--chipName', choices=['1-1_ohmic_e',
                                                '1-3_ohmic_e',
                                                '2-3_ohmic_e',
                                                '3-5_schottky_e',
                                                '2-4_schottky_h'],
                   help="Chip name. ['%default']",
                   default='3-5_schottky_e')

    parser.add_option_group(det)

    # Analyse ------------------------------

    analyse = optparse.OptionGroup(parser, TC.strc("Analyses",
                                                   'blue', 'bold'))

    analyse.add_option('-x', '--pixel', type='int', nargs=2,
                       help="Specific pixel to analyse/plot (row, column)." + \
                            " [%default]",
                       default=(0, 0))
    analyse.add_option("-A", "--analysis", choices=['spectra',
                                                    'homogeneity',
                                                    'stability',
                                                    'leakage'],
                       help="Analysis to perform/display ('spectra' | " + \
                            "'homogeneity' | 'stability' | 'leakage'). " + \
                            "['%default']",
                       default='homogeneity')

    parser.add_option_group(analyse)

    # Graphics and plots ------------------------------

    graph = optparse.OptionGroup(parser, TC.strc("Graphics and plots",
                                                 'blue', 'bold'))

    graph.add_option("-p", "--plot", action='store_true',
                     help="Plot flag (syn. '--graph=png')")
    graph.add_option('-g', '--graph',
                     choices=['eps', 'pdf', 'svg', 'png', 'pylab'],
                     help="Graphic output format (eps|pdf|svg|png|pylab)")

    graph.add_option("--dpi", type="int",
                     help="Figure DPI")

    parser.add_option_group(graph)

    # Verbose & debug ------------------------------

    verbose = optparse.OptionGroup(parser, TC.strc("Verbosity and debug",
                                                   'blue', 'bold'))

    verbose.add_option('-v', '--verbose',
                       choices=('DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'),
                       help="Minimum verbose level. ['%default']",
                       default='INFO')
    verbose.add_option('-D', '--debug', action='store_true',
                       help="Debug mode: Open an ipython embeded shell " +
                            "containing all the defined variables and " +
                            "functions.")

    parser.add_option_group(verbose)

    # Starting message & time =================================================

    ts = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    cts = TC.strc(ts, 'blue', 'bold')

    print("")
    TC.printc("cppm_analysis.py [{0}]".format(ts).center(72),
              'black', 'bold')
    TC.printc("clement.buton@synchrotron-soleil.fr".center(72), 'blue')

    # Check for options consistency ===========================================

    opts, args = parser.parse_args()

    # Logging
    logger = TC.logger(opts.verbose)

    # Debug mode (using IPython.embed)
    if opts.debug:
        logger.warning(TC.strc("DEBUG mode is ON !\n", 'black', 'bold'))
        from IPython import embed

    # Graphics
    if opts.graph:
        opts.plot = True
    elif opts.plot:
        opts.graph = 'pylab'

    # =========================================================================

    print(description + '\n')

    # Load data from "CPPM_23_08_13" repository ------------------------------

    # Amercium spectra analysis
    if opts.analysis == 'spectra':

        # Init variables
        initPath = os.path.join(opts.dataPath, opts.analysis, opts.chipName)
        root, dirs, files = os.walk(initPath).next()

        data = []
        for f in files:
            if not f.startswith('.'):
                spec = N.loadtxt(os.path.join(root, f))
                if opts.chipName == '3-5_schottky_e':
                    # data.append([spec[:, 1], spec[:, 2]]) # energy
                    data.append([spec[:, 0], spec[:, 2]])

                if opts.chipName == '1-1_ohmic_e':
                    data.append([spec[:, 0], spec[:, 1]])

                if opts.chipName == '2-4_schottky_h':
                    data.append([spec[:, 0], spec[:, 1]])

        data = N.array(data)

    if (opts.analysis == 'homogeneity') or (opts.analysis == 'stability'):

        # Init variables
        initPath = os.path.join(opts.dataPath, opts.analysis, opts.chipName)

        if (opts.chipName == '3-5_schottky_e') or (opts.chipName == '1-1_ohmic_e'):
            files = N.concatenate([f for r, d, f in os.walk(initPath)])
            dirs = N.squeeze([d for r, d, f in os.walk(initPath)])[0]
            roots = N.squeeze([r for r, d, f in os.walk(initPath)])[1:]
            idx = N.argsort(N.array([d.split('_')[-1]
                                     for d in dirs], dtype='i'))
            files = N.array([os.path.join(r, f) for r, f in zip(roots, files)
                             if not f.startswith('.')])[idx]

        else:
            files = N.concatenate([f for r, d, f in os.walk(initPath)])
            idx = N.argsort(N.array([os.path.splitext(f)[0].split('_')[-1]
                                     for f in files
                                     if not f.startswith('.')], dtype='i'))
            files = N.array([os.path.join(initPath, f) for f in files
                             if not f.startswith('.')])[idx]

        data = []
        for f in files:
            if (opts.chipName == '3-5_schottky_e') or (opts.chipName == '1-1_ohmic_e'):
                lines = open(f).readlines()[1:]
                data.append(N.array([float(line.split()[0])
                                     for line in lines]).reshape(120, 80))

            else:
                data.append(N.loadtxt(f)[:, 240:320])

        data = N.array(data)

    # Debug mode ==============================================================

    # Enter an embeded ipython shell containing all the defined
    # variables and functions (with the connections still alive)
    if opts.debug:
        logger.warning(TC.strc('Starting an embeded ipython shell...',
                               'black', 'bold'))
        embed()

    # Create output graphics ==================================================

    if opts.plot:
        TC.printc(" Graphics ".center(72, '-'), 'blue', 'bold')
        TC.printc("\nProducing output figures [%s]:" % opts.graph,
                  color='black', style='bold')

        import ToolBox.MPL as MPL
        from matplotlib.ticker import MaxNLocator, NullFormatter

        if opts.graph != 'pylab':
            backend, plot = MPL.get_backend(opts.graph,
                                            name=opts.chipName[4:].lower())

        import matplotlib.pyplot as P

        # Americium spectra -----------------------------

        if opts.analysis == 'spectra':

            figSpectra = P.figure(figsize=(16, 9), dpi=opts.dpi)
            figSpectra.subplots_adjust(0.08, 0.12, 0.95, 0.95)
            ax = figSpectra.add_subplot(1, 1, 1)

            if opts.chipName == '3-5_schottky_e':
                colors = [blue, green, orange, red]
                labels = ['-100 V', '-200 V', '-300 V', '-400 V']

            if opts.chipName == '1-1_ohmic_e':
                colors = [blue, green, red]
                labels = ['-100 V', '-150 V', '-200 V']

            if opts.chipName == '2-4_schottky_h':
                colors = [blue, green, red]
                labels = ['300 V', '400 V', '500 V']

            for d, c, l in zip(data, colors, labels):
                ax.plot(d[0], d[1], color=c, label=l, lw=2, alpha=0.7)

            # Legend
            if opts.chipName == '1-1_ohmic_e':
                ax.set_xlabel('Energy [Arbitrary Units]', fontsize=32)
                ax.set_xlim([20, 450])

            if opts.chipName == '2-4_schottky_h':
                ax.set_xlabel('Energy [Arbitrary units]', fontsize=32)
                ax.set_xlim([0, 2100])

            else:
                ax.set_xlabel('Energy [Arbitrary Units]', fontsize=32)
                ax.set_xlim([3, 65])

            ax.set_ylabel('Counts [Photons]', fontsize=32, labelpad=15)
            P.setp(ax.get_xticklabels(), fontsize=24)
            P.setp(ax.get_yticklabels(), fontsize=24)

            ax.legend(loc='best', prop=dict(size=24),
                      numpoints=2, frameon=False)

        # Homogeneity -----------------------------

        if opts.analysis == 'homogeneity':

            # Figure
            figHomogeneity = P.figure(figsize=(16, 9), dpi=opts.dpi)
            figHomogeneity.subplots_adjust(0., 0.13, 0.98, 0.95, wspace=0.25)

            # Subplots
            ax1 = figHomogeneity.add_subplot(1, 2, 1)
            ax2 = figHomogeneity.add_subplot(1, 2, 2)

            if opts.chipName == '3-5_schottky_e':
                test = data[143:170]
                a = N.mean(test[:5], axis=0)
                b = N.mean(test[-5:], axis=0)
                r = (-1000, 1000)

            if opts.chipName == '1-3_ohmic_e':
                test = data[150:400]
                # test = data[150:240]
                a = N.mean(test[:5], axis=0)
                b = N.mean(test[-5:], axis=0)
                r = (-1500, 1500)

            if opts.chipName == '2-4_schottky_h':
                test = data[164:176]
                a = N.mean(test[:3], axis=0)
                b = N.mean(test[-3:], axis=0)
                r = (-500, 500)

            c = b - a

            # Gaussian fit
            nbins = 1000
            y = N.histogram(N.concatenate(c),
                            bins=nbins,
                            range=(-1500, 1500))[0]
            x = N.linspace(-1500, 1500, len(y))

            import ToolBox.Optimizer as O
            from scipy.optimize import fmin_tnc

            M = O.Model(O.normal, jac=O.normal_jac, hess=O.normal_hess)
            D = O.DataSet(y=y, x=x)
            F = O.Fitter(M, D)

            guessPars = (y.max(), 0., 100.)
            tncPars, tncFCalls, tncRc = fmin_tnc(F.chi2, guessPars,
                                                 fprime=F.chi2_jac,
                                                 args=(x,), messages=0)
            dof, chi2, p = F.goodness(tncPars, x)
            pcov = F.covariance(tncPars, x)
            dy = M.errors(tncPars, pcov, x)

            # Image & color bar
            img = ax1.imshow(c, vmin=r[0], vmax=r[1],
                             aspect='equal',
                             interpolation='nearest',
                             origin='lower')

            from mpl_toolkits.axes_grid1 import make_axes_locatable
            divider = make_axes_locatable(ax1)
            cax = divider.append_axes("right", size="5%", pad=0.2)
            cb = figHomogeneity.colorbar(img, cax=cax)
            cb.set_label('Counting difference', fontsize=28)

            # Histogram & gaussian
            ax2.hist(N.concatenate(c), bins=nbins,
                     lw=3, color=blue, range=(-1500, 1500),
                     histtype='step', label='_nolegend_')
            l, = ax2.plot(x, O.normal(tncPars, x), lw=3,
                          color=red, label='-')

            b = ax2.errorband(x, O.normal(tncPars, x), dy, label='-',
                              ec=red, fc=red, alpha=0.5)

            # Labels, ticks & legend
            ax1.set_xlabel('X [pixels]', fontsize=28)
            ax1.set_ylabel('Y [pixels]', fontsize=28, labelpad=0)
            P.setp(ax1.get_xticklabels(), fontsize=20)

            P.setp(ax1.get_yticklabels(), fontsize=20)
            P.setp(cb.ax.get_yticklabels(), fontsize=20)
            P.rc('font', **{'size':'20'})

            ax2.set_xlabel('Counting difference', fontsize=28)
            P.setp(ax2.get_xticklabels(), fontsize=20)
            P.setp(ax2.get_yticklabels(), fontsize=20)
            P.rc('font', **{'size':'20'})

            ax2.legend([(l, b)], [u'μ=%.2f±%.2f\nσ=%.2f±%.2f' % \
                                  (tncPars[1], pcov[1, 1],
                                   tncPars[2], pcov[2,2])],
                       loc='best', frameon=False)

            # Number of points outside 1 sigma
            # print(len(c[(c >= 1 * tncPars[2]) | (c <= -1 * tncPars[2])]))

            # Pixel discrpancies  -----------------------------

            figDiscrepancies = P.figure(figsize=(12, 12), dpi=opts.dpi)
            figDiscrepancies.subplots_adjust(0.1, 0.1, 0.97, 0.97, wspace=0.2, hspace=0.2)
            ax1 = figDiscrepancies.add_subplot(4, 1, 1)
            ax2 = figDiscrepancies.add_subplot(4, 1, 2)
            ax3 = figDiscrepancies.add_subplot(4, 1, 3)
            ax4 = figDiscrepancies.add_subplot(4, 1, 4)

            ax1.plot(data[:, 0, 40], lw=2, color=blue, label='-')
            ax2.plot(data[:, 1, 40], lw=2, color=blue, label='-')
            ax3.plot(data[:, 10, 10], lw=2, color=blue, label='-')
            ax4.plot(data[:, 60, 60], lw=2, color=blue, label='-')

            ax1.xaxis.set_major_formatter(NullFormatter())    # 4 x-axis ticks
            ax1.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
            ax1.set_xlim([0, len(data)])
            ax1.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            ax1.text(0.1, 0.15,
                     '[%i,%i]' % (0, 40),
                     fontsize=28,
                     horizontalalignment='center',
                     verticalalignment='center',
                     transform=ax1.transAxes)

            ax2.xaxis.set_major_formatter(NullFormatter())    # 4 x-axis ticks
            ax2.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
            ax2.set_xlim([0, len(data)])
            ax2.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            ax2.text(0.1, 0.15,
                     '[%i,%i]' % (1, 40),
                     fontsize=28,
                     horizontalalignment='center',
                     verticalalignment='center',
                     transform=ax2.transAxes)

            ax3.set_ylabel(u'                            ∑ counts [photons]',
                           fontsize=28, labelpad=15)
            ax3.xaxis.set_major_formatter(NullFormatter())    # 4 x-axis ticks
            ax3.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
            ax3.set_xlim([0, len(data)])
            ax3.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            ax3.text(0.1, 0.15,
                     '[%i,%i]' % (10, 10),
                     fontsize=28,
                     horizontalalignment='center',
                     verticalalignment='center',
                     transform=ax3.transAxes)
            P.setp(ax3.get_xticklabels(), fontsize=20)
            P.setp(ax3.get_yticklabels(), fontsize=20)
            P.rc('font', **{'size':'20'})

            ax4.set_xlabel('# exposures', fontsize=28)
            ax4.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
            ax4.set_xlim([0, len(data)])
            ax4.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            ax4.text(0.1, 0.15,
                     '[%i,%i]' % (60, 60),
                     fontsize=28,
                     horizontalalignment='center',
                     verticalalignment='center',
                     transform=ax4.transAxes)
            P.setp(ax4.get_xticklabels(), fontsize=20)
            P.rc('font', **{'size':'20'})

        # Satbility -----------------------------

        if opts.analysis == 'stability':

            figStability = P.figure(figsize=(15, 5), dpi=opts.dpi)
            figStability.subplots_adjust(0.09, 0.19, 0.98, 0.93)
            ax = figStability.add_subplot(1, 1, 1)

            from ToolBox.Statistics import median_stats

            a = N.sum(data, axis=(1, 2))
            med, nmad = median_stats(a)
            b = N.delete(a, N.ravel(N.argwhere(a < med - nmad)))#[140:240]
            mean, std = N.mean(b), N.std(b)
            x = range(len(a))
            # x = range(140, 240)
            y = N.ones_like(x) * mean
            dy = N.ones_like(x) * std

            l = ax.axhline(mean, color=red, alpha=0.7, ls='--', lw=2, label='-')
            b = ax.errorband(x, y, dy, label='-', ec=red, fc=red, alpha=0.3)

            # ax.plot(N.sum(data[:240], axis=(1, 2)), lw=2, color=blue, label='-')
            ax.plot(N.sum(data, axis=(1, 2)), lw=2, color=blue, label='-')

            # Legend
            ax.yaxis.set_major_locator(MaxNLocator(3))    # 4 x-axis ticks
            ax.set_xlabel('# Exposures', fontsize=32)
            ax.set_ylabel(u'∑ counts [photons]', fontsize=32, labelpad=15.)
            ax.set_xlim([0, len(data)])
            ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            P.setp(ax.get_xticklabels(), fontsize=24)
            P.setp(ax.get_yticklabels(), fontsize=24)
            P.rc('font', **{'size':'24'})

            ax.legend([(l, b)], [u'mean = %1.2e ± %1.0e RMS' % (mean, std)],
                      loc='best', prop=dict(size=24), numpoints=2,
                      frameon=False)

        # Show & save plots ===================================================

        if opts.graph == 'pylab':
            P.show()

        else:
            # fig.savefig(plot + '_1', format=backend)
            P.close('all')

        print("")

    # Elapsed time ============================================================

    elapsed_time = datetime.datetime.now() - start_time
    minutes, seconds = str(elapsed_time).split(':')[-2:]
    TC.printc(" Done [%02i min %02i sec] ".center(76, '-') %
              (int(minutes), round(float(seconds))), 'blue', 'bold')

    # Acknowledgment ==========================================================

    TC.printc("""
If you have found this software useful for your research, we would
appreciate an acknowledgment citing 'the Xpad team at synchrotron SOLEIL
(2013)'.\n""", 'black', 'bold')

# End of cppm_analysis.py =====================================================
