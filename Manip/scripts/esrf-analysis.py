#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
## Filename:          esrf_analysis.py
## Description:       script description
## Author:            Clement Buton <clement.buton@synchrotron-soleil.fr>
## Author:            $Author: cbuton $
## Created on:        $Date: 2013/09/03 14:40:05 $
## Modified on:       2013/12/10 15:12:19
## Copyright:         2013, Clément Buton (CeCILL-C)
## $Id: esrf_analysis.py, 2013/09/03 14:40:05 cbuton Exp $
###############################################################################

"""
.. _esrf_analysis.py:

esrf_analysis.py
================

**Analysis of the data taken at Esrf on 16/08/2013.**

.. todo::
    * Correct bugs in 'shift' definition

"""

from __future__ import print_function  # be prepared for python 3.x

__author__ = 'Clement Buton <clement.buton@synchrotron-soleil.fr>'
__date__ = '2013/09/03 14:40:05'
__adv__ = 'esrf_analysis.py'

import os
import datetime
import numpy as N
import ToolBox.ColoredText as TC

from Calibration import libXpad as libX
from matplotlib.ticker import MaxNLocator

# Non-default colors
blue = '#0066CC'
red = '#CC0033'
green = '#009966'
orange = '#FF9900'
purple = '#FF00FF'

# MAIN ########################################################################

if __name__ == "__main__":

    start_time = datetime.datetime.now()  # start time

    import optparse

    # Options =================================================================

    usage = " usage: [%prog] [options] \n" \
            " [-g graph -p plot -V verbose -D debug --dpi] "

    description = TC.strc("""
Analysis of the chip stability from the data taken at Esrf on
12/09/2013.""", 'black', 'bold')

    parser = optparse.OptionParser(usage, description=description)

    # Inputs/Outputs ------------------------------

    io = optparse.OptionGroup(parser, TC.strc("Inputs/Outputs",
                                              'blue', 'bold'))

    io.add_option('-d', '--dataPath', type='string',
                  help="Data path. ['%default']",
                  default='/Users/cbuton/Projects/Xpad/Data/ESRF/ESRF_12_09_13')

    parser.add_option_group(io)

    # Detector ------------------------------

    det = optparse.OptionGroup(parser, TC.strc("Detector caracteristics",
                                               'blue', 'bold'))

    det.add_option('-s', '--chipShape', type='int', nargs=2,
                   help="Chip shape (rows, columns) in pixels. [%default]",
                   default=(120, 80))
    det.add_option('-n', '--chipName', choices=['1-3_ohmic_e',
                                                '2-5_schottky_h',
                                                '3-4_schottky_e'],
                   help="Chip name. ['%default']",
                   default='2-5_schottky_h')

    parser.add_option_group(det)

    # Analyse ------------------------------

    analyse = optparse.OptionGroup(parser, TC.strc("Analyses",
                                                   'blue', 'bold'))

    analyse.add_option('-x', '--pixel', type='int', nargs=2,
                       help="Specific pixel to analyse/plot (row, column). [%default]",
                       default=(0, 0))
    analyse.add_option("-A", "--analysis", choices=['stability','leakage'],
                       help="Analysis to perform/display ('stability' | 'leakage'). ['%default']",
                        default='stability')

    parser.add_option_group(analyse)

    # Graphics and plots ------------------------------

    graph = optparse.OptionGroup(parser, TC.strc("Graphics and plots",
                                                 'blue', 'bold'))

    graph.add_option("-p", "--plot", action='store_true',
                     help="Plot flag (syn. '--graph=png')")
    graph.add_option('-g', '--graph',
                     choices=['eps', 'pdf', 'svg', 'png', 'pylab'],
                     help="Graphic output format (eps|pdf|svg|png|pylab)")

    graph.add_option("--dpi", type="int",
                     help="Figure DPI")

    parser.add_option_group(graph)

    # Verbose & debug ------------------------------

    verbose = optparse.OptionGroup(parser, TC.strc("Verbosity and debug",
                                                   'blue', 'bold'))

    verbose.add_option('-v', '--verbose',
                       choices=('DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'),
                       help="Minimum verbose level. ['%default']",
                       default='INFO')
    verbose.add_option('-D', '--debug', action='store_true',
                       help="Debug mode: Open an ipython embeded shell " +
                            "containing all the defined variables and " +
                            "functions.")

    parser.add_option_group(verbose)

    # Starting message & time =================================================

    ts = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    cts = TC.strc(ts, 'blue', 'bold')

    print("")
    TC.printc("esrf_analysis.py [{0}]".format(ts).center(72),
              'black', 'bold')
    TC.printc("clement.buton@synchrotron-soleil.fr".center(72), 'blue')

    # Check for options consistency ===========================================

    opts, args = parser.parse_args()

    # Logging
    logger = TC.logger(opts.verbose)

    # Debug mode (using IPython.embed)
    if opts.debug:
        logger.warning(TC.strc("DEBUG mode is ON !\n", 'black', 'bold'))
        from IPython import embed

    # Graphics
    if opts.graph:
        opts.plot = True
    elif opts.plot:
        opts.graph = 'pylab'

    # =========================================================================

    print(description + '\n')

    # Load data from "ESRF_16_08_13" repository ------------------------------

    # Init variables
    detector = libX.Detector(opts.chipShape, 7, 1, name=opts.chipName)
    initPath = os.path.join(opts.dataPath, opts.chipName)
    acqPath = os.path.join(initPath, 'Acquisitions')

    # Stability ------------------------------

    data = []
    for root, dirs, files in os.walk(initPath):
        if os.path.split(root)[-1].startswith('Stabilite'):
            tmp = []
            idx = []
            for f in sorted(files):
                if not f.startswith('.'):
                    idx.append(int(os.path.splitext(f)[0].split('_')[-1]))
                    tmp.append(detector.get_chip_array(N.loadtxt(os.path.join(root, f)), 1, 4))
            data.append(N.array(tmp)[N.argsort(idx)])

    stabilities = [N.sum(d, axis=(1, 2)) for d in data]

    # Current leakage ------------------------------

    root, dirs, files = os.walk(initPath).next()
    leakages = [N.loadtxt(os.path.join(root, f), delimiter=',')
                for f in files if not f.startswith('.')]

    # Correlation ------------------------------

    # x = leakages[1][:, 0] - N.min(leakages[1][:, 0])
    # y = leakages[1][:, -1]

    # rm = [0, 1, 85, 170]        # N.argwhere(y<1.36) or x[y<1.36]
    # x = N.delete(x, rm)
    # y = N.delete(y, rm)

    # a = N.where((y > 1.36) & (y < 1.86), 1.5, y)
    # a[(a < 1.5)] = -1
    # a[(a == 1.5)] = 0
    # idx = N.ravel(N.argwhere(a == -1) - 1)[1:]
    # a[idx] = 1
    # a = N.where((a!=-1) & (a!=1), 0, a)

    # ix = [x[(x >= lims[0]) & (x <= lims[1])]
    #       for lims in zip(x[a==-1][:-1], x[a==1])]
    # iy = [y[(x >= lims[0]) & (x <= lims[1])]
    #       for lims in zip(x[a==-1][:-1], x[a==1])]

    # l = []
    # for ar in iy:
    #     if len(ar) == 82:
    #         ar = N.append(ar, ar[-1]) # approximation
    #         ar = N.insert(ar, 0, ar[0]) # approximation
    #         ar = ar.reshape(7, 12)
    #     if len(ar) == 83:
    #         ar = N.append(ar, ar[-1]) # approximation
    #         ar = ar.reshape(7, 12)
    #     elif len(ar) == 85:
    #         ar = ar[:-1]        # approximation
    #         ar = ar.reshape(7, 12)
    #     else:
    #         ar = ar.reshape(7, 12)
    #     l.append(N.mean(ar, axis=1))

    # l = N.ravel(l)[:-3]
    # s = stabilities[3]

    # Debug mode ==============================================================

    # Enter an embeded ipython shell containing all the defined
    # variables and functions (with the connections still alive)
    if opts.debug:
        logger.warning(TC.strc('Starting an embeded ipython shell...',
                               'black', 'bold'))
        embed()

    # Create output graphics ==================================================

    if opts.plot:
        TC.printc(" Graphics ".center(72, '-'), 'blue', 'bold')
        TC.printc("\nProducing output figures [%s]:" % opts.graph,
                  color='black', style='bold')

        import ToolBox.MPL as MPL

        if opts.graph != 'pylab':
            backend, plot = MPL.get_backend(opts.graph,
                                            name=opts.chipName[4:].lower())

        import matplotlib.pyplot as P

        # Stability plots -----------------------------

        if opts.analysis == 'stability':
            for i, stability in enumerate(stabilities):
                figStab = P.figure(figsize=(15, 7), dpi=opts.dpi)
                figStab.subplots_adjust(0.09, 0.15, 0.98, 0.96)
                ax = figStab.add_subplot(1, 1, 1)

                if i == 0:
                    ax.plot(N.arange(stability.size), stability,
                            color=blue, lw=2, label='_')

                    y1 = stability[:112]
                    x1 = N.arange(stability.size)[:112]
                    p = N.polyfit(x1, y1, 1)
                    idx = N.array(y1 / N.polyval(p, x1) > 0.982, dtype='bool')
                    y1 = y1[idx]
                    x1 = x1[idx]

                    y2 = stability[112:]
                    x2 = N.arange(stability.size)[112:]
                    p = N.polyfit(x2, y2, 1)
                    idx = N.array(y2 / N.polyval(p, x2) > 0.985, dtype='bool')
                    y2 = y2[idx]
                    x2 = x2[idx]

                    abs = [x1, x2]
                    ord = [y1, y2]

                if i == 1:
                    ord = [stability[:-1]]
                    abs = [N.arange(ord[0].size)]
                    ax.plot(abs[0], ord[0], color=blue, lw=2, label='_')

                if i == 2:
                    idx = N.array(stability > 1.45e7, dtype='bool')
                    abs = [N.arange(stability.size)[idx]]
                    ord = [stability[idx]]
                    ax.plot(N.arange(stability.size), stability,
                            color=blue, lw=2, label='_')

                if i == 3:
                    idx = N.array(stability > 1.33e7, dtype='bool')
                    abs = [N.arange(stability.size)[idx]]
                    ord = [stability[idx]]
                    ax.plot(N.arange(stability.size), stability,
                            color=blue, lw=2, label='_')

                for x, y in zip(abs, ord):
                    norm = N.mean(y)
                    y /= norm

                    # Fit
                    import ToolBox.Optimizer as O
                    from scipy.optimize import fmin_tnc

                    M = O.Model(O.linear, jac=O.linear_jac, hess=O.linear_hess)
                    D = O.DataSet(y=y, x=x)
                    F = O.Fitter(M, D)

                    guessPars = N.polyfit(x[:-1], y[:-1], 1)
                    tncPars, tncFCalls, tncRc = fmin_tnc(F.chi2, guessPars,
                                                         fprime=F.chi2_jac,
                                                         args=(x,), messages=0)
                    dof, chi2, p = F.goodness(tncPars, x)

                    y *= norm
                    tncPars *= norm

                    pcov = F.covariance(tncPars, x)
                    dy = M.errors(tncPars, pcov, x)

                    yerr = N.std(y / N.polyval(tncPars, x)) * norm

                    l, = ax.plot(x, N.polyval(tncPars, x),
                                 color=red, alpha=0.7, ls='--', lw=2, label='-')
                    b = ax.errorband(x, N.polyval(tncPars, x),
                                     yerr, label='-', ec='w', fc=red, alpha=0.3)

                # Legend
                ax.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
                ax.set_xlabel('# exposures', fontsize=32)
                ax.set_ylabel(u'∑ counts [photons]', fontsize=32, labelpad=15)
                if i == 1:
                    ax.set_xlim([0, stability.size - 2])
                else:
                    ax.set_xlim([0, stability.size - 1])
                ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
                P.setp(ax.get_xticklabels(), fontsize=24)
                P.setp(ax.get_yticklabels(), fontsize=24)
                P.rc('font', **{'size':'24'})

                ax.legend([(l, b)], ['RMS=%.1f%%' % (yerr / norm * 100)],
                          loc='best', frameon=False)


            # # Singe pixel analysis
            # figPixel = P.figure(figsize=(15, 7), dpi=opts.dpi)
            # figPixel.subplots_adjust(0.09, 0.15, 0.98, 0.96)
            # ax = figPixel.add_subplot(1, 1, 1)

            # stability = data[1][:, opts.pixel[0], opts.pixel[1]]
            # x = N.arange(len(stability))
            # x = N.ravel(zip(x, x + 1))
            # y = N.ravel(zip(stability, stability))

            # ax.plot(x, y, lw=1.5, color=blue)
            # ax.text(0.9, 0.9, 'pixel [%i,%i]' % (opts.pixel[0], opts.pixel[1]),
            #         fontsize=32,
            #         horizontalalignment='center',
            #         verticalalignment='center',
            #         transform=ax.transAxes)

            # # Legend
            # ax.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
            # ax.set_xlabel('# exposures', fontsize=32)
            # ax.set_ylabel(u'∑ counts', fontsize=32, labelpad=15)
            # P.setp(ax.get_xticklabels(), fontsize=24)
            # P.setp(ax.get_yticklabels(), fontsize=24)
            # P.rc('font', **{'size':'24'})

        # Current leakage plots -----------------------------

        if opts.analysis == 'leakage':
            for leakage in leakages:
                figLeak = P.figure(figsize=(15, 7), dpi=opts.dpi)
                figLeak.subplots_adjust(0.09, 0.15, 0.97, 0.96)
                ax = figLeak.add_subplot(1, 1, 1)

                x = N.ravel(zip(leakage[:, 0],
                                leakage[:, 0])) - N.min(leakage[:, 0])
                y = N.ravel(zip(leakage[:, -1], leakage[:, -1]))

                ax.plot(x, y, lw=1.5, color=blue)

                # Legend
                ax.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
                ax.set_xlabel('Time [s]', fontsize=32)
                ax.set_ylabel(u'Current [µA]', fontsize=32, labelpad=15)
                ax.ticklabel_format(style='plain')
                P.setp(ax.get_xticklabels(), fontsize=24)
                P.setp(ax.get_yticklabels(), fontsize=24)
                P.rc('font', **{'size':'24'})

        # Show & save plots ===================================================

        if opts.graph == 'pylab':
            P.show()

        else:
            # figStab.savefig(plot, format=backend)
            # figPixel.savefig(plot, format=backend)
            # figLeak.savefig(plot, format=backend)
            P.close('all')

        print("")

    # Elapsed time ============================================================

    elapsed_time = datetime.datetime.now() - start_time
    minutes, seconds = str(elapsed_time).split(':')[-2:]
    TC.printc(" Done [%02i min %02i sec] ".center(76, '-') %
              (int(minutes), round(float(seconds))), 'blue', 'bold')

    # Acknowledgment ==========================================================

    TC.printc("""
If you have found this software useful for your research, we would
appreciate an acknowledgment citing 'the Xpad team at synchrotron SOLEIL
(2013)'.\n""", 'black', 'bold')

# End of esrf_analysis.py =====================================================
