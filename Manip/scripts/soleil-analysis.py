#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
## Filename:          soleil_analysis.py
## Description:       script description
## Author:            Clement Buton <clement.buton@synchrotron-soleil.fr>
## Author:            $Author: cbuton $
## Created on:        $Date: 2013/09/03 14:40:05 $
## Modified on:       2014/01/21 23:28:21
## Copyright:         2013, Clément Buton (CeCILL-C)
## $Id: soleil_analysis.py, 2013/09/03 14:40:05 cbuton Exp $
###############################################################################

"""
.. _soleil_analysis.py:

soleil_analysis.py
==================

**Analysis of the data taken at Soleil on 16/08/2013.**

.. todo::
    * Correct bugs in 'shift' definition

"""

from __future__ import print_function  # be prepared for python 3.x

__author__ = 'Clement Buton <clement.buton@synchrotron-soleil.fr>'
__date__ = '2013/09/03 14:40:05'
__adv__ = 'soleil_analysis.py'

import os
import datetime
import numpy as N
import ToolBox.ColoredText as TC

from Calibration import libXpad as libX
from Calibration import libCalibration as libC

from ToolBox.Statistics import median_stats

# Non-default colors
blue = '#0066CC'
red = '#CC0033'
green = '#009966'
orange = '#FF9900'
purple = '#FF00FF'

# Definitions =================================================================

def shift(a, shift, axis=None):
    """Shift array elements along a given axis. Elements that roll
     beyond the last position are re-introduced at the first as zero.

    :param a: array_like
    :param shift: The number of places by which elements are shifted.
    :param axis: The axis along which elements are shifted.  By default,
                 the array is flattened before shifting, after which the
                 original shape is restored.

    .. warning:: Does not work yet with negative shift (to the left) or
                 with axis different than None or 0.

    .. todo:: find a solution to be able to have an array of shifts
    """

    a = N.asanyarray(a)
    if axis is None:
        n = a.size
        reshape = True

    else:
        n = a.shape[axis]
        reshape = False

    if shift >= 0:
        right = True

    else:
        shift = -shift
        right = False

    shift %= n
    indexes = N.concatenate((N.arange(n - shift, n), N.arange(n - shift)))
    res = a.take(indexes, axis)

    if right:
        res[:N.arange(n - shift, n).size] = 0

    else:
        res[-N.arange(n - shift, n).size:] = 0

    if reshape:
        return res.reshape(a.shape)

    else:
        return res

# MAIN ########################################################################

if __name__ == "__main__":

    start_time = datetime.datetime.now()  # start time

    import optparse

    # Options =================================================================

    usage = " usage: [%prog] [options] \n" \
            " [-g graph -p plot -V verbose -D debug --dpi] "

    description = TC.strc("""
Analysis of the energy range of a DAC from the data taken at Soleil on
16/08/2013.""", 'black', 'bold')

    parser = optparse.OptionParser(usage, description=description)

    # Inputs/Outputs ------------------------------

    io = optparse.OptionGroup(parser, TC.strc("Inputs/Outputs",
                                              'blue', 'bold'))

    io.add_option('-d', '--dataPath', type='string',
                  help="Data path. ['%default']",
                  default='/Users/cbuton/Projects/Xpad/Data/Soleil/SOLEIL_16_08_13')

    io.add_option('--adjust', action="store_true",
                  help="If True, perform the intensity adjustment with respect "
                       "to the physical description of the detector. [%default]",
                  default=False)

    parser.add_option_group(io)

    # Detector ------------------------------

    det = optparse.OptionGroup(parser, TC.strc("Detector caracteristics",
                                               'blue', 'bold'))

    det.add_option('--chipShape', type='int', nargs=2,
                   help="Chip shape (rows, columns) in pixels. [%default]",
                   default=(120, 80))
    det.add_option('--chipName', choices=['1-3_Ohmique_e',
                                          '2-5_Schottky_Trous',
                                          '3-2_Schottky_e'],
                   help="Chip name. ['%default']",
                   default='2-5_Schottky_Trous')

    parser.add_option_group(det)

    # Threshold selection method ------------------------------

    method = optparse.OptionGroup(parser,
                                  TC.strc("Threshold determination methods",
                                          'blue', 'bold'))

    method.add_option('-l', '--level', type='float',
                      help="Approximated 'plateau' level (in counts). [%default]",
                      default=1500.)
    method.add_option('-n', '--non_uniformity', type='float',
                      help="Non uniformity factor increasing the 'plateau' level "
                           "(for bad flat fields). [%default]",
                      default=3.)

    method.add_option('--min_points', type='int',
                      help="Minimum number of non zero points requiered "
                           "in a single pixel scan to select a decent "
                           "threshold. [%default]",
                      default=1)
    method.add_option('--min_counts', type='int',
                      help="Minimum number of counts requiered "
                           "in a single pixel scan to select a decent "
                           "threshold. [%default]",
                      default=100)

    parser.add_option_group(method)

    # Analyse ------------------------------

    analyse = optparse.OptionGroup(parser, TC.strc("Analyses",
                                                   'blue', 'bold'))

    analyse.add_option('-x', '--pixel', type='int', nargs=2,
                       help="Specific pixel to analyse/plot (row, column). [%default]",
                       default=(0, 0))
    analyse.add_option("-A", "--analysis", choices=['linearity','calibration','acquisition'],
                       help="Analysis to perform/display ('linearity' | 'calibration' | 'acquisition'). ['%default']",
                       default='calibration')

    parser.add_option_group(analyse)

    # Graphics and plots ------------------------------

    graph = optparse.OptionGroup(parser, TC.strc("Graphics and plots",
                                                 'blue', 'bold'))

    graph.add_option("-p", "--plot", action='store_true',
                     help="Plot flag (syn. '--graph=png')")
    graph.add_option('-g', '--graph',
                     choices=['eps', 'pdf', 'svg', 'png', 'pylab'],
                     help="Graphic output format (eps|pdf|svg|png|pylab)")

    graph.add_option("--dpi", type="int",
                     help="Figure DPI")

    parser.add_option_group(graph)

    # Verbose & debug ------------------------------

    verbose = optparse.OptionGroup(parser, TC.strc("Verbosity and debug",
                                                   'blue', 'bold'))

    verbose.add_option('-v', '--verbose',
                       choices=('DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'),
                       help="Minimum verbose level. ['%default']",
                       default='INFO')
    verbose.add_option('-D', '--debug', action='store_true',
                       help="Debug mode: Open an ipython embeded shell " +
                            "containing all the defined variables and " +
                            "functions.")

    parser.add_option_group(verbose)

    # Starting message & time =================================================

    ts = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    cts = TC.strc(ts, 'blue', 'bold')

    print("")
    TC.printc("soleil_analysis.py [{0}]".format(ts).center(72),
              'black', 'bold')
    TC.printc("clement.buton@synchrotron-soleil.fr".center(72), 'blue')

    # Check for options consistency ===========================================

    opts, args = parser.parse_args()

    # Logging
    logger = TC.logger(opts.verbose)

    # Level
    if (opts.non_uniformity >= 1.):
        opts.level *= opts.non_uniformity # Prevent bad flat field

    # Debug mode (using IPython.embed)
    if opts.debug:
        logger.warning(TC.strc("DEBUG mode is ON !\n", 'black', 'bold'))
        from IPython import embed

    # Graphics
    if opts.graph:
        opts.plot = True
    elif opts.plot:
        opts.graph = 'pylab'

    # =========================================================================

    print(description + '\n')

    # Load data from "SOLEIL_16_08_13" repository ------------------------------

    # Init variables
    module = libX.Detector(opts.chipShape, 7, 1, name=opts.chipName)
    initPath = os.path.join(opts.dataPath, opts.chipName, 'Courbes_en_S')
    directories = os.walk(initPath).next()[1]
    energies, exposures = N.array([d.split('_')[-2:] for d in directories]).T

    energies = N.array([e.replace('keV', '').replace('-', '.')
                        for e in energies], dtype='f')
    exposures = N.array([e.replace('s', '') for e in exposures], dtype='f')

    # Retrieve the data
    data = N.array([module.get_chip_array(N.loadtxt(os.path.join(root, f)),
                                            M=1, C=4)
                    for root, dirs, files in os.walk(initPath)
                    if root.endswith('DACL_scan')
                    for f in sorted(files, key=lambda x: os.stat(os.path.join(root, x)).st_mtime)
                    if not f.startswith('.')])

    npts = energies.size
    nscans = len(data) / npts
    npixels = N.product(opts.chipShape)
    nrows, ncols = opts.chipShape[0], opts.chipShape[1]

    data = data.reshape((npts, nscans, nrows, ncols)) # (8, 64, 120, 80)

    # Adjust  the  input  pixel's   counts  according  to  the  physical
    # description of the detector
    if opts.adjust:
        chip = libX.Detector(opts.chipShape, 1, 1, name=opts.chipName)
        tmp = []
        for d in data:
            for img in d:
                chip.set_data(array=img)
                chip.adjust_data()
                tmp.append(chip.data)
        data = N.array(tmp).reshape((npts, nscans, nrows, ncols))

    # Shift the S-curves for all energies so they can be easily compared
    # with each other on a given pixel (x, y)
    maximums = N.argmax(data, axis=1)
    shifted = N.array([shift(data[n, :, i, j], nscans - maximums[n, i, j] - 1)
                       for n in range(npts)
                       for i in range(nrows)
                       for j in range(ncols)]).reshape((npts,
                                                        nrows,
                                                        ncols,
                                                        nscans))
    shifted = shifted.swapaxes(1, -1).swapaxes(2, 3)

    # Perform the thresholds selection ------------------------------

    intensities = N.arange(nscans)

    # On straight data
    if opts.analysis == 'calibration':
        thresholds = N.array([libC.Threshold(intensities,
                                             array,
                                             method='adhoc',
                                             scan='dacl',
                                             level=opts.level,
                                             min_points=opts.min_points,
                                             min_counts=opts.min_counts,
                                             verbose=False)
                              for array in data])

        for threshold in thresholds:
            threshold.get_pixel_thresholds()

        dacl = N.array([threshold.dacl for threshold in thresholds])
        cmaps = N.array([threshold.cmap for threshold in thresholds])

    # On shifted data
    if opts.analysis == 'linearity':
        shifted_thresholds = N.array([libC.Threshold(intensities,
                                                     array,
                                                     level=opts.level,
                                                     min_points=opts.min_points,
                                                     min_counts=opts.min_counts,
                                                     verbose=False)
                                      for array in shifted])

        for threshold in shifted_thresholds:
            threshold.get_pixel_thresholds()

        dacl = N.array([threshold.dacl
                        for threshold in shifted_thresholds])
        cmaps = N.array([threshold.cmap
                         for threshold in shifted_thresholds])

    # Acquisition data ------------------------------

    if opts.analysis == 'acquisition':
        initPath = os.path.join(opts.dataPath, opts.chipName, 'Acqui')
        root, dirs, files = os.walk(initPath).next()

        cmaps = N.array([module.get_chip_array(N.loadtxt(os.path.join(root, f)), M=1, C=4)
                         for f in files
                         if 'Clem' in f.split('_')
                         if 'diode' not in f.split('_')])

        # Trim real outliers
        meds, nmads = N.array([median_stats(cmap) for cmap in cmaps]).T
        for (med, nmad, cmap) in zip(meds, nmads, cmaps):
            cmap[cmap > med + 100 * nmad] = 0

        npts = len(cmaps)
        dacl = N.zeros_like(cmaps)

    # Initite the 'Detector' instances ------------------------------

    detectors = [libX.Detector(opts.chipShape, 1, 1, name=opts.chipName)
                 for n in xrange(npts)]

    for (det, d, c) in zip(detectors, dacl, cmaps):
        det.set_data(d, dacl=True)  # Set dacl
        det.set_data(c)             # Set counts @ threshold

    # Debug mode ==============================================================

    # Enter an embeded ipython shell containing all the defined
    # variables and functions (with the connections still alive)
    if opts.debug:
        logger.warning(TC.strc('Starting an embeded ipython shell...',
                               'black', 'bold'))
        embed()

    # Create output graphics ==================================================

    if opts.plot:
        TC.printc(" Graphics ".center(72, '-'), 'blue', 'bold')
        TC.printc("\nProducing output figures [%s]:" % opts.graph,
                  color='black', style='bold')

        import ToolBox.MPL as MPL
        from matplotlib.ticker import MaxNLocator

        if opts.graph != 'pylab':
            backend, plot = MPL.get_backend(opts.graph,
                                            name=opts.chipName[4:].lower())

        import matplotlib.pyplot as P

        cmap = P.matplotlib.cm.jet(N.linspace(0, 1, N.unique(energies).size))
        colors = {}
        for i, e in enumerate(N.unique(energies)):
            colors['%i' % e] = cmap[i]

        if opts.analysis == 'linearity':

            # Data at each energy -----------------------------

            fig1 = P.figure(figsize=(12, 8), dpi=opts.dpi)
            fig1.subplots_adjust(0.1, 0.2, 0.95, 0.95)
            ax = fig1.add_subplot(1, 1, 1)

            for energy, scan in zip(energies,
                                    data[:, :, opts.pixel[0], opts.pixel[1]]):
                ax.plot(intensities, scan,
                        color=colors['%i' % energy], lw=1.5, alpha=0.7,
                        label='%ikeV' % energy)

                # Legend
                ax.set_xlabel('Intensities [DACL]', fontsize=32)
                ax.set_ylabel('Counts [photons]', fontsize=32)
                P.setp(ax.get_xticklabels(), fontsize=24)
                P.setp(ax.get_yticklabels(), fontsize=24)
                P.rc('font', **{'size':'24'})

            ax.legend(loc='best', prop=dict(size=24),
                      numpoints=2, frameon=False)

            # Shifted data at each energy and their threshold -----------------

            fig2 = P.figure(figsize=(12, 8), dpi=opts.dpi)
            fig2.subplots_adjust(0.06, 0.13, 0.97, 0.95)

            ax = fig2.add_subplot(1, 1, 1)

            for energy, scan, dac in zip(energies,
                                         shifted[:, :, opts.pixel[0], opts.pixel[1]],
                                         dacl[:, opts.pixel[0], opts.pixel[1]]):
                ax.plot(intensities, scan,
                        color=colors['%i' % energy], lw=1.5, alpha=0.7,
                        label='%ikeV' % energy)
                ax.axvline(dac, color=colors['%i' % energy], ls='--',
                           label='__nolegend__')

                # Legend
                ax.set_xlabel('Intensities [DACL]', fontsize=32)
                ax.set_ylabel('Counts [photons]', fontsize=32)
                ax.set_xlim([dacl[:, opts.pixel[0], opts.pixel[1]].min() - 3, 63])
                P.setp(ax.get_xticklabels(), fontsize=24)
                P.setp(ax.get_yticklabels(), fontsize=24)
                P.rc('font', **{'size':'24'})

            ax.legend(loc='best', prop=dict(size=24),
                      numpoints=2, frameon=False)

            # Relation threshold vs energy (for a chosen pixel) ---------------

            fig3 = P.figure(figsize=(12, 8), dpi=opts.dpi)
            fig3.text(0.5, 0.92, '%s [pixel (%i,%i)]' % (opts.chipName,
                                                         opts.pixel[0],
                                                         opts.pixel[1]),
                      ha='center', fontsize=24)

            ax = fig3.add_subplot(1, 1, 1,
                                  xlabel='Energy [keV]',
                                  ylabel='Intensities [DACL]')

            # Points
            ax.scatter(energies, dacl[:, opts.pixel[0], opts.pixel[1]],
                       s=60, edgecolors=blue, facecolors='none', lw=2,
                       label='dacl values')

            # Fit
            p = N.polyfit(energies, dacl[:, opts.pixel[0], opts.pixel[1]], 1)
            ind = N.argsort(N.polyval(p, energies))

            ax.plot(energies[ind], N.polyval(p, energies)[ind],
                    color=red, lw=1.5,
                    label='y = %.2f * x + %.2f' % (p[0], p[1]))

            # Legend
            ax.legend(loc='best', prop=dict(size=16),
                      numpoints=2, frameon=False)

            # Relation threshold vs energy (all pixels) -----------------------

            fig4 = P.figure(figsize=(15, 5), dpi=opts.dpi)
            fig4.subplots_adjust(0.07, 0.19, 0.98, 0.94)
            ax = fig4.add_subplot(1, 1, 1)

            # Points
            med, nMAD = median_stats(dacl.reshape((npts, npixels)), axis=-1)
            ax.errorbar(energies, med, nMAD, marker='o', ms=10,
                        mec=blue, mfc=blue, mew=2,
                        ecolor=blue, capsize=0, ls='none', label=u'median ± nMAD')

            # Linear regression
            import ToolBox.Optimizer as O
            from scipy.optimize import fmin_tnc

            M = O.Model(O.linear, jac=O.linear_jac, hess=O.linear_hess)
            D = O.DataSet(y=med, x=energies, dy=nMAD)
            F = O.Fitter(M, D)

            guessPars = N.polyfit(energies, med, 1)
            tncPars, tncFCalls, tncRc = fmin_tnc(F.chi2, guessPars,
                                                 fprime=F.chi2_jac,
                                                 args=(energies,), messages=0)
            dof, chi2, p = F.goodness(tncPars, energies)

            logger.info("linearity probability: %.2f" % p)

            pcov = F.covariance(tncPars, energies)
            dy = M.errors(tncPars, pcov, energies)

            l, = ax.plot(energies[ind], N.polyval(tncPars, energies)[ind],
                         color=red, alpha=0.7, ls='--', lw=2, label='-')
            b = ax.errorband(energies[ind], N.polyval(tncPars, energies)[ind],
                             dy[ind], label='-', ec='w', fc=red, alpha=0.3)

            # Limits
            ax.axis([7, 41, 27, 61])

            # Legend
            ax.yaxis.set_major_locator(MaxNLocator(5))    # 4 x-axis ticks
            ax.set_xlabel('Energy [keV]', fontsize=32)
            ax.set_ylabel('Intensities [DACL]', fontsize=32, labelpad=15)
            P.setp(ax.get_xticklabels(), fontsize=24)
            P.setp(ax.get_yticklabels(), fontsize=24)
            P.rc('font', **{'size':'24'})

            ax.legend([(l, b)], [u'slope=%.2f ± %.2f' % \
                                 (tncPars[0], pcov[0, 0])],
                      loc='best', frameon=False)

        # Calibration maps & histogram -----------------------------

        if opts.analysis == 'calibration':

            for det in detectors:

                # Figure
                fig = P.figure(figsize=(23, 7))
                fig.subplots_adjust(0.03, 0.13, 0.98, 0.97, wspace=0.23)

                # Subplots
                ax1 = fig.add_subplot(1, 3, 1)
                ax2 = fig.add_subplot(1, 3, 2)
                ax3 = fig.add_subplot(1, 3, 3, autoscale_on=True, aspect='auto')

                # Images
                ax1, cb1, img1 = det.plot(ax=ax1, dacl=True)
                ax2, cb2, img2 = det.plot(ax=ax2)

                # Histogram
                tmp = N.unique(det.dacl[N.isfinite(det.dacl)])
                lims = (tmp.min(), tmp.max())
                nbins = lims[1] - lims[0]
                ax3.hist(det.dacl[N.isfinite(det.dacl)],
                         histtype='stepfilled',
                         lw=0, color=blue,
                         bins=nbins, range=lims)

                # Legend
                ax1.set_xlabel('X [pixels]', fontsize=28)
                ax1.set_ylabel('Y [pixels]', fontsize=28, labelpad=0)
                cb1.set_label('Intensities [DACL]', fontsize=28)
                P.setp(ax1.get_xticklabels(), fontsize=20)
                P.setp(ax1.get_yticklabels(), fontsize=20)
                P.setp(cb1.ax.get_yticklabels(), fontsize=20)

                ax2.set_xlabel('X [pixels]', fontsize=28)
                ax2.set_ylabel('Y [pixels]', fontsize=28, labelpad=0)
                cb2.set_label('Counts [Photons]', fontsize=28)
                P.setp(ax2.get_xticklabels(), fontsize=20)
                P.setp(ax2.get_yticklabels(), fontsize=20)
                P.setp(cb2.ax.get_yticklabels(), fontsize=20)

                ax3.xaxis.set_major_locator(MaxNLocator(5))    # 6 x-axis ticks
                ax3.yaxis.set_major_locator(MaxNLocator(4))    # 5 x-axis ticks
                # ax3.set_yticks(ax3.get_ylim())
                ax3.set_xlabel('Intensities [DACL]', fontsize=28)
                ax3.set_ylabel('# counts', fontsize=28)
                ax3.set_xlim(lims)
                P.setp(ax3.get_xticklabels(), fontsize=20)
                P.setp(ax3.get_yticklabels(), fontsize=20)

        # Acquisition of calibrated images -----------------------------

        if opts.analysis == 'acquisition':
            for det in detectors:

                # Figure
                figAcq = P.figure(figsize=(16, 9))
                figAcq.subplots_adjust(0., 0.13, 0.98, 0.95, wspace=0.25)

                # Subplots
                ax1 = figAcq.add_subplot(1, 2, 1)
                ax2 = figAcq.add_subplot(1, 2, 2)

                # Images
                ax1, cb1, img1 = det.plot(ax=ax1, vmax=1.7e5)

                # Histogram
                data = N.concatenate(det.data)
                data = data[N.isfinite(data)]
                # lims = (data.min(), data.max())
                lims = (data.min(), 1.7e5)
                nbins = 500
                y, x, patch = ax2.hist(data,
                                       lw=3, color=blue,
                                       histtype='step',
                                       bins=nbins, range=lims)

                # Gaussian fit
                import ToolBox.Optimizer as O
                from scipy.optimize import fmin_tnc

                x = N.linspace(lims[0], lims[1], len(y))
                M = O.Model(O.normal, jac=O.normal_jac, hess=O.normal_hess)
                D = O.DataSet(y=y, x=x)
                F = O.Fitter(M, D)

                guessPars = (y.max(), x[N.argmax(y)], N.std(x))
                tncPars, tncFCalls, tncRc = fmin_tnc(F.chi2, guessPars,
                                                     fprime=F.chi2_jac,
                                                     args=(x,), messages=0)
                dof, chi2, p = F.goodness(tncPars, x)
                pcov = F.covariance(tncPars, x)
                dy = M.errors(tncPars, pcov, x)

                l, = ax2.plot(x, O.normal(tncPars, x),
                              color=red, ls='-', lw=3, label='-')

                b = ax2.errorband(x, O.normal(tncPars, x), dy,
                                  label='-', ec='w', fc=red, alpha=0.5)

                # Labels, ticks & legend -----------------------------

                ax1.set_xlabel('X [pixels]', fontsize=28)
                ax1.set_ylabel('Y [pixels]', fontsize=28, labelpad=0)
                cb1.set_label('Counts [photons]', fontsize=28)
                P.setp(ax1.get_xticklabels(), fontsize=20)
                P.setp(ax1.get_yticklabels(), fontsize=20)
                P.setp(cb1.ax.get_yticklabels(), fontsize=20)
                P.rc('font', **{'size':'20'})

                ax2.xaxis.set_major_locator(MaxNLocator(4))    # 5 x-axis ticks
                ax2.yaxis.set_major_locator(MaxNLocator(4))    # 5 x-axis ticks
                ax2.set_xlabel('Counts [photons]', fontsize=28)
                ax2.set_xlim(lims)
                ax2.set_ylim([0, y.max()])
                P.setp(ax2.get_xticklabels(), fontsize=20)
                P.setp(ax2.get_yticklabels(), fontsize=20)
                P.rc('font', **{'size':'20'})

                ax2.legend([(l, b)], [u'μ=%.1f±%.1f\nσ=%.1f±%.1f' % \
                                      (tncPars[1], pcov[1, 1],
                                       N.absolute(tncPars[2]), pcov[2,2])],
                           loc='best', frameon=False)

                # Cross Patterns -----------------------------

                if opts.chipName == '1-3_Ohmique_e':

                    from mpl_toolkits.axes_grid1 import ImageGrid

                    # Figure
                    figPix = P.figure(figsize=(10, 8))
                    grid = ImageGrid(figPix, 111,
                                     axes_pad=0.2,
                                     label_mode = "L",
                                     nrows_ncols=(2,2),
                                     cbar_mode="single",
                                     cbar_size="5%",
                                     cbar_pad=0.2)

                    figPix.subplots_adjust(0.05, 0.1, 0.95, 0.95,
                                           wspace=0.1, hspace=0.1)

                    # Coordinates
                    maps = N.array([cmaps[0][76:81, 23:28],
                                    cmaps[0][18:23, 27:32],
                                    cmaps[0][99:104, 30:35],
                                    cmaps[0][34:39, 57:62]])

                    coords = [[79,26],
                              [21,30],
                              [102,33],
                              [37,60]]

                    for ax, m, xy in zip(grid, maps, coords):
                        im = ax.imshow(m,
                                       vmin=cmaps.min(),
                                       vmax=cmaps.max(),
                                       aspect='equal',
                                       interpolation='nearest')
                        ax.text(0.05, 0.9,
                                '[%i,%i]' % (xy[0], xy[1]),
                                fontsize=28,
                                horizontalalignment='left',
                                verticalalignment='center',
                                transform=ax.transAxes)

                    cb = ax.cax.colorbar(im)
                    ax.cax.toggle_label(True)

                    grid[0].set_xticklabels([])
                    grid[0].set_yticklabels([])
                    grid[1].set_xticklabels([])
                    grid[1].set_yticklabels([])
                    grid[2].set_xticklabels([])
                    grid[2].set_yticklabels([])

                    grid[0].set_ylabel("Y [pixels]", fontsize=28)
                    grid[2].set_xlabel("X [pixels]", fontsize=28)
                    grid[2].set_ylabel("Y [pixels]", fontsize=28)
                    grid[3].set_xlabel("X [pixels]", fontsize=28)
                    cb.set_label_text('Counts [photons]', fontsize=28)
                    P.rc('font', **{'size':'20'})


                    # ax1.set_xlabel('X [pixels]', fontsize=28)
                    # ax1.set_ylabel('Y [pixels]', fontsize=28, labelpad=0)
                    # cb1.set_label('Counts [photon]', fontsize=28)
                    # P.setp(ax1.get_xticklabels(), fontsize=20)
                    # P.setp(ax1.get_yticklabels(), fontsize=20)
                    # P.setp(cb1.ax.get_yticklabels(), fontsize=20)
                    # P.rc('font', **{'size':'20'})

        # Show & save plots ===================================================

        if opts.graph == 'pylab':
            P.show()

        else:
            fig1.savefig(plot + '_1', format=backend)
            fig2.savefig(plot + '_2', format=backend)
            fig3.savefig(plot + '_3', format=backend)
            fig4.savefig(plot + '_4', format=backend)
            P.close('all')

        print("")

    # Elapsed time ============================================================

    elapsed_time = datetime.datetime.now() - start_time
    minutes, seconds = str(elapsed_time).split(':')[-2:]
    TC.printc(" Done [%02i min %02i sec] ".center(76, '-') %
              (int(minutes), round(float(seconds))), 'blue', 'bold')

    # Acknowledgment ==========================================================

    TC.printc("""
If you have found this software useful for your research, we would
appreciate an acknowledgment citing 'the Xpad team at synchrotron SOLEIL
(2013)'.\n""", 'black', 'bold')

# End of soleil_analysis.py ===================================================
