.. Xpad documentation master file, created by
   sphinx-quickstart on Fri Jun 20 09:55:35 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _Xpad:

================================================
     Xpad Calibration Project Documentation
================================================

:Version: |version| of |today|
:Authors: Clément Buton <`clement.buton@synchrotron-soleil.fr`>
:Copyright: 2014, CHiPSpeCT

.. highlight:: python
   :linenothreshold: 3

.. Make interpreted `text` acts as literal ``text``
.. default-role:: literal

.. include:: ../README.txt

.. toctree::
   :maxdepth: 2
